### About me
Hi, i am spacenautx

----
### completed

- [Software Engineer Certificate](https://www.hackerrank.com/certificates/5daa8e0e81c6)
- [Google Advanced Data Analytics](https://www.coursera.org/account/accomplishments/specialization/certificate/GAVMSTRS67QP)
- [IBM Quantum Challenge: Spring 2023](https://www.credly.com/badges/4b826a17-e8d8-401e-aaa5-4d93b5090cc6/print)
- [Ultimate rust crash course](https://www.udemy.com/certificate/UC-141e1b33-545f-4ed6-b1d3-2f08d2bf31d2/)
- [PyBites White Ninja](https://codechalleng.es/badge/3b79e7d9-4f6e-48b0-b903-94020ee2bc0d)
- [Pandas Kaggle](https://www.kaggle.com/learn/certification/shyamsam/pandas)
- [Google IT Automation](https://www.coursera.org/account/accomplishments/specialization/certificate/RKDD55LSQ6FC)
- [CS50 Harvard](https://certificates.cs50.io/7055a027-ce4e-40b5-9a57-ab41ea31a805.pdf?size=letter)
- [Securing Software Helsinki](https://certificates.mooc.fi/validate/5d2dr0qd25l)
- [Hackerrank](https://www.hackerrank.com/certificates/26450a34ed97)
- [Hackthebox](https://app.hackthebox.com/profile/5925)
- [Freecodecamp Information Security](https://www.freecodecamp.org/certification/shyamsam/information-security-v7)

----
### repo
- [notes](https://gitlab.com/spacenautx/notes)
- [Kaggle](https://gitlab.com/spacenautx/kaggle)

----
### interests
- linux
- python
- security

----
### stats

<!--START_SECTION:waka-->
<!--END_SECTION:waka-->
